const express = require("express")
const router = express.Router()
const Product = require("../models/product")

router.route("/products").get(async (req, res) => {
    try {
        const products = await Product.find(req.body.filter).sort(req.body.sort)
        res.status(200).json(products)
    }
    catch (err) {
        res.status(500).json({ message: err.message })
    }
});

router.route("/products").post(async (req, res) => {
    const {name, price, description, amount, unit} = req.body
    const check = await Product.findOne( {name} )
    if(check) {
        return res.status(400).json( {message: "Product already exists"} )
    }

    const product = new Product({
        name: name,
        price: price,
        description: description,
        amount: amount,
        unit: unit
    })
    try{
        const newProduct = await product.save()
        res.status(201).json(newProduct)
    } catch (err) {
        res.status(400).json( { message: err.message} )
    }
});

router.route("/products/:id").put(getProduct, async (req, res) => {
    try {
        await res.product.updateOne({
            name: req.body.name,
            price: req.body.price,
            description: req.body.description,
            amount: req.body.amount,
            unit: req.body.unit
        })
        res.status(200).json( {message: "Succesfully updated"} )
    } catch (err) {
        res.status(500).json( {message: err.message} )
    }
});

router.route("/products/:id").delete(getProduct, async (req, res) => {
    try {
        await res.product.deleteOne()
        res.json( {message: "Deleted Product"})
    } catch (err) {
        res.status(500).json( {message: err.message} )
    }
});

router.route("/report").get(async (req, res) => {
    try {
        const report = await Product.aggregate([
            {$project: {
                "_id": 0,
                "name": 1,
                "amount": 1,
                "total_value": {$multiply: ["$price", "$amount"]}
            }}
        ])
        res.status(200).json(report)
    } catch (err) {
        res.status(500).json( {message: err.message} )
    }
});

async function getProduct(req, res, next) {
    let product
    try{
        product = await Product.findById(req.params.id)
        if (product == null){
            return res.status(404).json( {message: "Cannot find product"} )
        }
    } catch (err) {
        return res.status(404).json( {message: err.message} )
    }

    res.product = product
    next()
}

module.exports = router