require("dotenv").config()

const port = process.env.PORT

const express = require("express")
const app = express()
const mongoose = require("mongoose")

mongoose.connect(process.env.DATABASE_URI)
const db = mongoose.connection
db.on("error", (error) => console.log(error))
db.once("open", () => console.log("Connected to database"))

app.use(express.json())
app.use(require("./routes/products"))

app.listen(port, () => console.log(`Server Started on port ${port}`))